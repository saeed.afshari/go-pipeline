module gitlab.com/saeed.afshari/go-pipeline

go 1.13

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/gobuffalo/packr/v2 v2.8.0
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/karrick/godirwalk v1.15.5 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/sirupsen/logrus v1.5.0 // indirect
	github.com/spf13/cobra v0.0.7 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.2
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59 // indirect
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200331124033-c3d80250170d // indirect
	golang.org/x/tools v0.0.0-20200402205330-226fa68e9d42 // indirect
)
