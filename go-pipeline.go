package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/saeed.afshari/go-pipeline/controller"
	"gitlab.com/saeed.afshari/go-pipeline/util"
	"os"
)

func main() {
	initializeViper()

	initializeControllers()
}

func initializeControllers() {
	r := gin.Default()

	controller.RegisterHandlers(r)

	err := r.Run(fmt.Sprintf(":%s", viper.GetString("server.port")))
	if err != nil {
		panic(err)
	}
}

func initializeViper() {
	env := "local"
	if len(os.Args) > 1 {
		env = os.Args[1]
	}
	err := util.InitializeViper(&env)
	if err != nil {
		panic(err)
	}
}
