package util

import (
	"bytes"
	"github.com/gobuffalo/packr/v2"
	"github.com/spf13/viper"
)

const (
	applicationConfigLocal = "application-local.yaml"
	applicationProd        = "application-prod.yaml"
	applicationStaging     = "application-stg.yaml"
)

//InitializeViper initialize viper to read configs from yaml files
func InitializeViper(env *string) error {
	box := packr.New("configs", "../resource")

	var configName = ""
	if *env == "local" {
		configName = applicationConfigLocal
	} else if *env == "prod" {
		configName = applicationProd
	} else if *env == "stg" {
		configName = applicationStaging
	} else {
		return nil
	}
	applicationEnv, err := box.FindString(configName)
	if err != nil {
		return err
	}

	viper.SetConfigType("yaml")

	return viper.ReadConfig(bytes.NewBuffer([]byte(applicationEnv)))
}
