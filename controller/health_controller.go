package controller

import (
	"github.com/gin-gonic/gin"
)

//GetHealth implements health check end-point functionality
func GetHealth(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "200",
	})
}
