package controller

import "github.com/gin-gonic/gin"

//RegisterHandlers registers all end-points
func RegisterHandlers(r *gin.Engine) {
	r.GET("/health", GetHealth)
}
