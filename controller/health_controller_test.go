package controller_test

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/saeed.afshari/go-pipeline/controller"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetHealth(t *testing.T) {
	r := gin.Default()
	controller.RegisterHandlers(r)

	w := httptest.NewRecorder()

	req, err := http.NewRequest("GET", "/health", nil)
	r.ServeHTTP(w, req)

	if w.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			w.Code, http.StatusOK)
	}

	if err != nil {
		t.Fatal(err)
	}

	expected := `{"message":"200"}`
	if w.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			w.Body.String(), expected)
	}
}
