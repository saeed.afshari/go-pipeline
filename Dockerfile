FROM golang:1.14.1-alpine

ARG SRC_PATH=/usr/local/go/src/go-pipeline
ARG DEST_PATH=/usr/local/go/src
ARG APP_NAME=gopipeline.out

COPY . $SRC_PATH

WORKDIR $SRC_PATH

#RUN apk update && apk add --no-cache git
RUN go get
RUN go get -u github.com/gobuffalo/packr/v2/packr2
RUN packr2 clean
RUN GOOS=linux GOARCH=amd64 packr2
RUN go build
RUN packr2 clean
RUN mv ./go-pipeline $DEST_PATH/$APP_NAME
RUN rm -rf $SRC_PATH

COPY ./resource $SRC_PATH

RUN $DEST_PATH/$APP_NAME prod

EXPOSE 8080 8080